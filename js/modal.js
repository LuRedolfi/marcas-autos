$(function(){
    		$('#sesion').on('show.bs.modal',function(e){
    			console.log('el modal se esta mostrando');

    			$('#sesionBtn').removeClass('btn-outline-success');
    			$('#sesionBtn').addClass('btn-primary');
    			$('#sesionBtn').prop('disabled',true);
    		});
    		$('#sesion').on('shown.bs.modal',function(e){
    			console.log('el modal se mostro');
    		});
    		$('#sesion').on('hide.bs.modal',function(e){
    			console.log('el modal se esta ocultando');
    		});
    		$('#sesion').on('hidden.bs.modal',function(e){
    			console.log('el modal se oculto');
    			$('#sesionBtn').removeClass('btn-primary');
    			$('#sesionBtn').addClass('btn-outline-success');
    			$('#sesionBtn').prop('disabled',false);
    		});
    	});